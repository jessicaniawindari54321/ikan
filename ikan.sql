-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2019 at 06:00 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ikan`
--

-- --------------------------------------------------------

--
-- Table structure for table `alat tangkap`
--

CREATE TABLE `alat tangkap` (
  `alatTangkap` varchar(100) NOT NULL,
  `namaAlattangkap` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alat tangkap`
--

INSERT INTO `alat tangkap` (`alatTangkap`, `namaAlattangkap`) VALUES
('T0001', 'Jaring'),
('T0002', 'Tombak'),
('T0003', 'Joran'),
('T0004', 'Kait ikan'),
('T0005', 'Benang pancing'),
('T0006', 'Pemberat pancing'),
('T0007', 'Umpan'),
('T0008', 'Jebakan ikan'),
('T0009', 'Indikator gigitan'),
('T0010', 'Jaring lingkar'),
('T0011', 'Pukat tarik'),
('T0012', 'Penggaruk'),
('T0013', 'Jaring angkat'),
('T0014', 'Gill net'),
('T0015', 'Pukat hela');

-- --------------------------------------------------------

--
-- Table structure for table `detail tangkapan`
--

CREATE TABLE `detail tangkapan` (
  `penangkapan_Id` char(5) NOT NULL,
  `ikan_Id` char(5) NOT NULL,
  `jumlahIkan` varchar(100) NOT NULL,
  `totalberat_Ikan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail tangkapan`
--

INSERT INTO `detail tangkapan` (`penangkapan_Id`, `ikan_Id`, `jumlahIkan`, `totalberat_Ikan`) VALUES
('PI001', 'N0001', '5', '35kg');

-- --------------------------------------------------------

--
-- Table structure for table `ikan`
--

CREATE TABLE `ikan` (
  `ikan_Id` char(5) NOT NULL,
  `jenisikan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ikan`
--

INSERT INTO `ikan` (`ikan_Id`, `jenisikan`) VALUES
('N0001', 'Tongkol'),
('N0002', 'Layur'),
('N0003', 'Dori'),
('N0004', 'Tuna'),
('N0005', 'Barakuda'),
('N0006', 'Salmon'),
('N0007', 'Kerapu');

-- --------------------------------------------------------

--
-- Table structure for table `kapal`
--

CREATE TABLE `kapal` (
  `nomorKapal` varchar(100) NOT NULL,
  `user_Id` int(5) NOT NULL,
  `alatTangkap` varchar(100) NOT NULL,
  `jenisKapal` varchar(50) NOT NULL,
  `ABK` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kapal`
--

INSERT INTO `kapal` (`nomorKapal`, `user_Id`, `alatTangkap`, `jenisKapal`, `ABK`) VALUES
('K0001', 2, 'T0008', 'Honda', '4');

-- --------------------------------------------------------

--
-- Table structure for table `pelayaran`
--

CREATE TABLE `pelayaran` (
  `pelayaran_Id` char(5) NOT NULL,
  `nomorKapal` varchar(100) NOT NULL,
  `wilayah_Tangkap` varchar(100) NOT NULL,
  `tanggal_Waktu` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelayaran`
--

INSERT INTO `pelayaran` (`pelayaran_Id`, `nomorKapal`, `wilayah_Tangkap`, `tanggal_Waktu`) VALUES
('P0001', 'K0001', 'Ancol', '2015-05-12'),
('P0002', 'K0001', 'Pluit', '2016-05-14');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_Id` char(5) NOT NULL,
  `nama_Role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_Id`, `nama_Role`) VALUES
('R0001', 'Admin'),
('R0002', 'Nelayan'),
('R0003', 'Kepala TPI'),
('R0004', 'Petugas Lelang'),
('R0005', 'Staff Dinas Perikanan'),
('R0006', 'Kepala Dinas Perikanan');

-- --------------------------------------------------------

--
-- Table structure for table `tangkapan`
--

CREATE TABLE `tangkapan` (
  `penangkapan_Id` char(5) NOT NULL,
  `pelayaran_Id` char(5) NOT NULL,
  `waktuInput` date NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tangkapan`
--

INSERT INTO `tangkapan` (`penangkapan_Id`, `pelayaran_Id`, `waktuInput`, `status`) VALUES
('PI001', 'P0001', '2015-05-12', 'Approved'),
('PI002', 'P0002', '2016-05-14', 'Approved');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_Id` int(5) NOT NULL,
  `role_Id` char(5) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_Id`, `role_Id`, `nama`, `gender`, `nohp`, `email`, `password`) VALUES
(1, 'R0001', 'Andrew', 'Laki-Laki', '2147483647', '', ''),
(2, 'R0002', 'Jane', 'Perempuan', '2147483647', '', ''),
(3, 'R0003', 'Matthew', 'Laki-Laki', '2147483647', '', ''),
(4, 'R0004', 'Cinta', 'Perempuan', '2147483647', '', ''),
(5, 'R0005', 'Rosa', 'Perempuan', '2147483647', '', ''),
(6, 'R0006', 'Nita', 'Perempuan', '2147483647', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alat tangkap`
--
ALTER TABLE `alat tangkap`
  ADD PRIMARY KEY (`alatTangkap`);

--
-- Indexes for table `detail tangkapan`
--
ALTER TABLE `detail tangkapan`
  ADD PRIMARY KEY (`penangkapan_Id`);

--
-- Indexes for table `ikan`
--
ALTER TABLE `ikan`
  ADD PRIMARY KEY (`ikan_Id`);

--
-- Indexes for table `kapal`
--
ALTER TABLE `kapal`
  ADD PRIMARY KEY (`nomorKapal`);

--
-- Indexes for table `pelayaran`
--
ALTER TABLE `pelayaran`
  ADD PRIMARY KEY (`pelayaran_Id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_Id`);

--
-- Indexes for table `tangkapan`
--
ALTER TABLE `tangkapan`
  ADD PRIMARY KEY (`penangkapan_Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<html>

<head>
    <title>Ikan-ku</title>
    <!-- menghubungkan dengan file css -->
    <link rel="stylesheet" type="text/css" href="halaman_nelayanStyle.css">
    <!-- menghubungkan dengan file jquery -->
    <script type="text/javascript" src="jquery.js"></script>
</head>

<body>

    <div class="content">
        <header>
            <h1 class="judul">IKAN</h1>
            <h3 class="deskripsi">Selamat Datang!</h3>
        </header>

        <div class="menu">
            <ul>
                <li><a href="LandingPage_nelayan.php">HOME</a></li>
                <li><a href="LandingPage_nelayan.php">TENTANG</a></li>
                <li><a href="nelayan_index.php">TANGKAPAN</a></li>
            </ul>
        </div>

        <div class="badan">


            <?php
            if (isset($_GET['page'])) {
                $page = $_GET['page'];

                switch ($page) {
                    case 'home':
                        include "home.php";
                        break;
                    case 'tentang':
                        include "tentang.php";
                        break;
                    case 'tangkapan':
                        include "tangkapan.php";
                        break;
                    default:
                        echo "<center><h3>Maaf. Halaman tidak di temukan !</h3></center>";
                        break;
                }
            } else {
                include "home.php";
            }

            ?>

        </div>
    </div>
</body>

</html>